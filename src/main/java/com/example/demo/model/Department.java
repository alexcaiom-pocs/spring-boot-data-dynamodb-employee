/**
 * 
 */
package com.example.demo.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author alex
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class Department {

	@DynamoDBAttribute
	private String departmentName;
	
	
	@DynamoDBAttribute
	private String departmentCode;
	
	
	
}
