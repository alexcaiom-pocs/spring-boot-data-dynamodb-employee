/**
 * 
 */
package com.example.demo.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author alex
 *
 */
@SpringBootConfiguration
@ConfigurationProperties(prefix = "aws.dynamo-db")
@Data
@Slf4j
public class DynamoDBConfiguration {

	private String serviceEndpoint;
	private String signingRegion;
	private String accessKey;
	private String secretKey;

	@Bean
	public DynamoDBMapper dynamoDBMapper() {
		return new DynamoDBMapper(dynamoDB());
	}

	private AmazonDynamoDB dynamoDB() {
		log.info("[" + extracted()
				+ " - dynamoDB] - starting with values serviceEndpoint: {}, signingRegion: {}, accessKey: {}, secretKey: {}", serviceEndpoint, signingRegion, accessKey, secretKey);
		return AmazonDynamoDBClientBuilder
				.standard()
				.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(serviceEndpoint, signingRegion))
				.withCredentials(new AWSStaticCredentialsProvider(
						new BasicAWSCredentials(accessKey, secretKey)
						))
				.build();
	}

	private String extracted() {
		return getClass().getSimpleName();
	}
	
}
