/**
 * 
 */
package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;

/**
 * @author alex
 *
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeRepository repository;
	
	@PostMapping
	public Employee save(@RequestBody Employee employee) {
		return repository.save(employee);
	}
	
	@GetMapping("/{id}")
	public Employee get(@PathVariable String id) {
		return repository.getEmployeeById(id);
	}
	
	@DeleteMapping("/{id}")
	public String delete(@PathVariable String id) {
		return repository.delete(id);
	}
	
	@PutMapping("/{id}")
	public String update(@PathVariable String id, @RequestBody Employee employee) {
		return repository.update(id, employee);
	}
	
}
