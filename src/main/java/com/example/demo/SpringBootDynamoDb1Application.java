package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDynamoDb1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDynamoDb1Application.class, args);
	}

}
