/**
 * 
 */
package com.example.demo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.example.demo.model.Employee;

/**
 * @author alex
 *
 */
@Repository
public class EmployeeRepository {

	@Autowired
	private DynamoDBMapper mapper;
	
	public Employee save(Employee employee) {
		mapper.save(employee);
		return employee;
	}
	
	public Employee getEmployeeById(String employeeId) {
		return mapper.load(Employee.class, employeeId);
	}
	
	public String delete(String employeeId) {
		Employee emp = mapper.load(Employee.class, employeeId);
		mapper.delete(emp);
		return "Employee deleted!";
	}
	
	public String update (String employeeId, Employee employee) {
		mapper.save(employee, 
				new DynamoDBSaveExpression()
				.withExpectedEntry("employeeId", 
						new ExpectedAttributeValue(
								new AttributeValue()
								.withS(employeeId)
								)
						)
				);
		return employeeId;
	}
	
}
